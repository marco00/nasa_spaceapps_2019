﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HTR_App.Repositories
{
	public interface IDataService<T> where T : class
	{
		void Add(T record);

		T Get(Expression<Func<T, bool>> condition);
		List<T> GetRange(Expression<Func<T, bool>> condition);

		void Update(T newObject);

		void Delete(Expression<Func<T, bool>> condition);

		void DeleteMultiple(Expression<Func<T, bool>> condition);

		List<T> GetAllContents();
	}
}
