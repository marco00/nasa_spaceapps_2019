﻿using System;
using System.Collections.Generic;
using System.Text;
using HTR_App.Models;
using HTR_App.Repositories.LocalRepository;

namespace HTR_App.Repositories
{
	public interface IRepository
	{
		IDataService<User> Teacher { get; } 
		IDataService<Student> Student { get; } 
		IDataService<Test> Test { get; } 
		IDataService<TestScore> TestScore { get; } 
		IDataService<Subject> Subject { get; }
		IDataService<Teacher_Student> Teacher_Student { get; }
		IDataService<Class_Student> Class_Student { get; }
		IDataService<Test_Student> Test_Student { get; }
		
	}
}
