﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using HTR_App.Models;
using SQLite;

namespace HTR_App.Repositories.LocalRepository
{
	public class LocalDataService<T> : IDataService<T> where T : class, new()
	{
		private static string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "HTRAppDatabase.db3");
		public LocalDataService()
		{
		}

		public List<T> GetAllContents()
		{
			using (var db = new SQLiteConnection(path))
			{
				return db.Table<T>().ToList();

			}
		}
		public void Add(T record)
		{
			using (var db = new SQLiteConnection(path))
			{
				db.Insert(record);
			}
		}

		public T Get(Expression<Func<T, bool>> condition)
		{
			using (var db = new SQLiteConnection(path))
			{
				return db.Table<T>().FirstOrDefault(condition);
			}
			
		}

		public List<T> GetRange(Expression<Func<T, bool>> condition)
		{
			using (var db = new SQLiteConnection(path))
			{
				return db.Table<T>().Where(condition).ToList();
			}
			
		}
		
		public void Update(T newObject)
		{
			using (var db = new SQLiteConnection(path))
			{
				db.Update(newObject);
			}
			
		}

		public void Delete(Expression<Func<T, bool>> condition)
		{
			using (var db = new SQLiteConnection(path))
			{
				var objectToDelete = db.Table<T>().FirstOrDefault(condition);
				db.Delete(objectToDelete);
			}
			
		}
		public void DeleteMultiple(Expression<Func<T, bool>> condition)
		{
			using (var db = new SQLiteConnection(path))
			{
				var objectToDelete = db.Table<T>().Where(condition).ToList();
				foreach (var v in objectToDelete)
				{
					db.Delete(v);
				}
			}

		}


	}
}
