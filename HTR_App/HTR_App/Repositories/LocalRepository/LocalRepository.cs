﻿using System;
using System.Collections.Generic;
using System.Text;
using HTR_App.Models;

namespace HTR_App.Repositories.LocalRepository
{
	public class LocalRepository : IRepository
	{
		public LocalRepository()
		{
			
		}
		public IDataService<User> Teacher { get; } = new LocalDataService<User>();
		public IDataService<Student> Student { get; } = new LocalDataService<Student>();
		public IDataService<Test> Test { get; } = new LocalDataService<Test>();
		public IDataService<TestScore> TestScore { get; } = new LocalDataService<TestScore>();
		public IDataService<Subject> Subject { get; } = new LocalDataService<Subject>();
		public IDataService<Teacher_Student> Teacher_Student { get; } = new LocalDataService<Teacher_Student>();
		public IDataService<Class_Student> Class_Student { get; } = new LocalDataService<Class_Student>();
		public IDataService<Test_Student> Test_Student { get; } = new LocalDataService<Test_Student>();
	}
}
