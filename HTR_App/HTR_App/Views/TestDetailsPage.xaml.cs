﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;
using HTR_App.ViewModels;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	

	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TestDetailsPage : ContentPage
	{
		private TestDetailsViewModel vm;
		public TestDetailsPage ()
		{
			InitializeComponent ();
		}
		public TestDetailsPage (object[] parameters)
		{
			InitializeComponent();
			vm = App.Locator.TestDetailsViewModel;
			vm.User = (User)parameters[0];
			vm.SelectedTest = (Test)parameters[1];
			BindingContext = vm;
			vm.IsBusy = false;
			vm.LoadTestScores();

			AbsoluteLayout.SetLayoutFlags(activityIndicator, AbsoluteLayoutFlags.PositionProportional);
			AbsoluteLayout.SetLayoutBounds(activityIndicator, new Rectangle(0.5, 0.4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			vm.LoadTestScores();
		}

		private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			vm.GotoUpdateTestScore();
		}

		public bool DoIHaveInternet()
		{
			if (!CrossConnectivity.IsSupported)
				return true;

			//Do this only if you need to and aren't listening to any other events as they will not fire.
			var connectivity = CrossConnectivity.Current;

			try
			{
				return connectivity.IsConnected;
			}
			finally
			{
				CrossConnectivity.Dispose();
			}

		}

		private async void Button_OnClicked(object sender, EventArgs e)
		{
			MediaFile file;
			await CrossMedia.Current.Initialize();

			if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
			{
				await DisplayAlert("No Camera", ":( No camera available.", "OK");
				return;
			}

			if (DoIHaveInternet() == false)
			{
				await DisplayAlert("No title", "Can't read picture", "Ok");
				var parameters = new object[] { vm.SelectedTest, vm.User, new List<string>(), 0, 0 };
				vm.NavigationService.NavigateTo(ViewModelLocator.AddNewTestScorePage, parameters, false);
			}
			else
			{
				file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
				{
					Directory = "Sample",
					Name = "_test.jpg"
				});

				if (file == null)
					return;


				//await DisplayAlert("File Location", file.Path, "OK");
				var htrService = new HTRService();
				//TODO ENABLE THIS AFTER TESTING

				vm.IsBusy = true;
				var result = await htrService.UploadAndRecognizeImageAsync(file.Path);

				//TODO USE THIS FOR TESTING

				//var result = JsonConvert.DeserializeObject<TextOperationResult>(
				//	"{\r\n  \"status\": \"Succeeded\",\r\n  \"succeeded\": true,\r\n  \"failed\": false,\r\n  \"finished\": true,\r\n  \"recognitionResult\": {\r\n    \"lines\": [\r\n      {\r\n        \"boundingBox\": [\r\n          289,\r\n          489,\r\n          1154,\r\n          512,\r\n          1151,\r\n          612,\r\n          286,\r\n          589\r\n        ],\r\n        \"text\": \"Marco Pack 14. Pulido\",\r\n        \"words\": [\r\n          {\r\n            \"boundingBox\": [\r\n              295,\r\n              489,\r\n              522,\r\n              501,\r\n              508,\r\n              597,\r\n              280,\r\n              590\r\n            ],\r\n            \"text\": \"Marco\"\r\n          },\r\n          {\r\n            \"boundingBox\": [\r\n              556,\r\n              502,\r\n              716,\r\n              509,\r\n              703,\r\n              601,\r\n              542,\r\n              598\r\n            ],\r\n            \"text\": \"Pack\"\r\n          },\r\n          {\r\n            \"boundingBox\": [\r\n              770,\r\n              511,\r\n              911,\r\n              516,\r\n              898,\r\n              605,\r\n              757,\r\n              602\r\n            ],\r\n            \"text\": \"14.\"\r\n          },\r\n          {\r\n            \"boundingBox\": [\r\n              964,\r\n              518,\r\n              1172,\r\n              524,\r\n              1160,\r\n              608,\r\n              952,\r\n              606\r\n            ],\r\n            \"text\": \"Pulido\"\r\n          }\r\n        ]\r\n      },\r\n      {\r\n        \"boundingBox\": [\r\n          1519,\r\n          495,\r\n          1690,\r\n          584,\r\n          1659,\r\n          665,\r\n          1474,\r\n          569\r\n        ],\r\n        \"text\": \"10 / 10\",\r\n        \"words\": [\r\n          {\r\n            \"boundingBox\": [\r\n              1503,\r\n              486,\r\n              1580,\r\n              526,\r\n              1540,\r\n              603,\r\n              1463,\r\n              563\r\n            ],\r\n            \"text\": \"10\"\r\n          },\r\n          {\r\n            \"boundingBox\": [\r\n              1575,\r\n              523,\r\n              1632,\r\n              553,\r\n              1592,\r\n              630,\r\n              1535,\r\n              600\r\n            ],\r\n            \"text\": \"/\"\r\n          },\r\n          {\r\n            \"boundingBox\": [\r\n              1611,\r\n              542,\r\n              1694,\r\n              585,\r\n              1653,\r\n              662,\r\n              1571,\r\n              619\r\n            ],\r\n            \"text\": \"10\"\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  }\r\n}");


				vm.ScanImage(result);
				//var result = cvAPI.Scan(file.GetStream());
				//var result = ExtractLocalTextAsync("/storage/emulated/0/Android/data/com.companyname.HTR_App/files/Pictures/Sample/test.jpg");
				//vm.TestImageSource = file.Path;

			}


		}

		private async void MenuItem_OnClicked(object sender, EventArgs e)
		{
			var answer = await DisplayAlert("Delete test", "Are you sure?", "Yes", "No");
			if (answer == false) return;

			vm.DeleteTest();

		}
		//	const string subscriptionKey = "1455e47165be4dd694cc2afea2368ed1";

		//const string uriBase =
		//	"https://southeastasia.api.cognitive.microsoft.com/";

		

	}
}