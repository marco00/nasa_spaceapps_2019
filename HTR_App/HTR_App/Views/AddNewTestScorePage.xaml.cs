﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddNewTestScorePage : ContentPage
	{
		private AddTestScoreViewModel vm;
		public AddNewTestScorePage (object[] parameters)
		{
			InitializeComponent();
			var test = (Test)parameters[0];
			var user = (User)parameters[1];
			vm = App.Locator.AddTestScoreViewModel;
			BindingContext = vm;

			vm.ShowSuggestionsListIsVisible = true;
			vm.SuggestedStudentsInString = (ObservableCollection<string>)parameters[2];
			vm.TestScore = new TestScore();
			vm.TestScore.Score = (int)parameters[3];
			vm.TestScore.NumberOfItems = (int)parameters[4];



			vm.User = user;
			vm.SelectedTest = test;


			vm.ConvertStringStudentsToActualStudents();



			vm.LoadStudents();

			vm.StudentList.Clear();
			foreach (var v in vm.SuggestedStudents)
			{
				vm.StudentList.Add(v);
			}

			
		}

		

		private async void Button_OnClicked(object sender, EventArgs e)
		{
			if (vm.TestScore.Score > vm.TestScore.NumberOfItems || vm.TestScore.NumberOfItems == 0)
			{
				await DisplayAlert("Invalid score", "Review the score and try again", "Ok");
				return;
			}
			vm.AddTestScoreProc();
			await DisplayAlert("", "New score added!", "Ok");
		}
	}
}