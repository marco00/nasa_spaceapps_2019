﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StudentDetailsPage : ContentPage
	{
		StudentDetailsViewModel vm = App.Locator.StudentDetailsViewModel;

		public StudentDetailsPage (object[] parameters)
		{
			InitializeComponent ();
			vm.User = (User) parameters[0];
			vm.SelectedStudent = (Student) parameters[1];
			BindingContext = vm;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
		}

		private async void MenuItem_OnClicked(object sender, EventArgs e)
		{
			var answer = await DisplayAlert("Delete student", "Are you sure?", "Yes", "No");
			if (answer == false) return;

			vm.DeleteStudent();
		}
	}
}