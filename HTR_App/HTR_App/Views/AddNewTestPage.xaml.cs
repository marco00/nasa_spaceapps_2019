﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddNewTestPage : ContentPage
	{
		public AddNewTestPage ()
		{
			InitializeComponent ();
		}

		private AddTestViewModel vm;
		public AddNewTestPage(object[] parameters)
		{
			InitializeComponent();
			var vm = App.Locator.AddTestViewModel;
			vm.TestToBeAdded = new Test();
			vm.User = (User)parameters[0];
			vm.SelectedSubject = (Subject) parameters[1];
			BindingContext = vm;
		}

		private void Button_OnClicked(object sender, EventArgs e)
		{
			vm.AddNewTestProc();
			DisplayAlert("", "New test added!", "Ok");
		}
	}
}