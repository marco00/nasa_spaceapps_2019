﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddNewStudentPage : ContentPage
	{
		private AddStudentViewModel vm;
		public AddNewStudentPage (object[] parameters)
		{
			InitializeComponent ();
			vm =  App.Locator.AddStudentViewModel;
			vm.User = (User) parameters[0];
			vm.SelectedSubject = (Subject) parameters[1];
			vm.StudentToBeAdded = new Student();
			vm.ContactNumber = "";

			BindingContext = vm;
		}

		private void Button_OnClicked(object sender, EventArgs e)
		{
		vm.AddNewStudentProc();
			DisplayAlert("", "New student added!", "Ok");
		}
	}
}