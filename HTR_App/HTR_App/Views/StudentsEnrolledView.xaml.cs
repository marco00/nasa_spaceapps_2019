﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StudentsEnrolledView : ContentPage
	{
		private StudentsListViewModel vm;
		public StudentsEnrolledView (object[] parameters)
		{
			InitializeComponent();
			vm = App.Locator.StudentsListViewModel;
			vm.User = (User) parameters[0];
			vm.SelectedSubject = (Subject) parameters[1];
			vm.LoadStudents();

			BindingContext = vm;

		}

		private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			vm.GotoStudentDetailsviewProc();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			vm.LoadStudents();
		}
		//public StudentsEnrolledView(object[] parameters)
		//{
		//	var user = (User)parameters[0];
		//	var subject = (SelectedSubject)parameters[1];
		//	InitializeComponent();
		//	vm = App.Locator.ClassDetailsViewModel;
		//	vm.User = user;
		//	vm.SelectedSubject = subject;
		//	vm.Tests = new ObservableCollection<Test>(vm.testOperationsService.GetTests(subject));

		//	BindingContext = vm;

		//}

	}
}