﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UpdateTestScorePage : ContentPage
	{
		private UpdateTestScoreViewModel vm;
		public UpdateTestScorePage (object[] parameters)
		{
			InitializeComponent ();
			vm = App.Locator.UpdateTestScoreViewModel;
			vm.User = (User)parameters[0];
			vm.OldTestScore = (TestScore)parameters[1];
			vm.LoadTest();
			vm.NewTestScore = vm.OldTestScore.ShallowCopy();
			vm.Student = vm.GetStudentFromId();
			BindingContext = vm;
		}

		private async void MenuItem_OnClickedcked(object sender, EventArgs e)
		{
			var answer = await DisplayAlert("Delete test score", "Are you sure?", "Yes", "No");
			if (answer == false) return;

			vm.DeleteTestScore();
		}
	}
}