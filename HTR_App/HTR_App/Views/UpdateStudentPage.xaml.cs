﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UpdateStudentPage : ContentPage
	{
		public UpdateStudentPage()
		{
			InitializeComponent ();

		}
		public UpdateStudentPage (object[] parameters) 
		{
			InitializeComponent ();
			var vm = App.Locator.UpdateStudentViewModel;
			vm.User = (User) parameters[0];
			var student = (Student) parameters[1];
			vm.SelectedStudent = student;
			vm.StudentToReplaceOldStudent = student.ShallowCopy();
			BindingContext = vm;
		}
	}
}