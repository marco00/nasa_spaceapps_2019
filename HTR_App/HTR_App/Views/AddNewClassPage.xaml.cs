﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Views;
using HTR_App.Models;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddNewClassPage : ContentPage
	{
		public AddNewClassPage()
		{
			InitializeComponent();
		}
		AddClassViewModel vm;
		public AddNewClassPage(User user)
		{
			InitializeComponent();
			vm = App.Locator.AddClassViewModel;
			vm.User = user;
			BindingContext = vm;
			
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			vm.InitializeFields();
		}


		private void Button_OnClicked(object sender, EventArgs e)
		{
			vm.AddNewClassProc();
			DisplayAlert("", "New subject added!", "Ok");
		}
	}
}