﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UpdateSubjectPage : ContentPage
	{
		public UpdateSubjectPage ()
		{
			InitializeComponent ();
		}
		public UpdateSubjectPage(object[] parameters)
		{
			InitializeComponent();
			var vm = App.Locator.UpdateClassViewModel;
			vm.User = (User)parameters[0];
			vm.SelectedSubject = (Subject) parameters[1];
			vm.SubjectToReplaceOldSubject = vm.SelectedSubject.ShallowCopy();
			BindingContext = vm;
		}

	}
}