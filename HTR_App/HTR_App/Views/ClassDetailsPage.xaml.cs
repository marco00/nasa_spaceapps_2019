﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HTR_App.ViewModels;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ClassDetailsPage : ContentPage
	{
		private ClassDetailsViewModel vm;
		public ClassDetailsPage ()
		{
			InitializeComponent ();
		}
		public ClassDetailsPage(object[] parameters)
		{
			var user = (User) parameters[0];
			var subject = (Subject) parameters[1];
			InitializeComponent();
			vm = App.Locator.ClassDetailsViewModel;
			vm.User = user;
			vm.SelectedSubject = subject;
			BindingContext = vm;

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			vm.LoadTests();
		}

		private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			vm.GotoTestDetailsPageProc();
		}

		private async void MenuItem_OnClicked(object sender, EventArgs e)
		{
			var answer = await DisplayAlert("Delete class", "Are you sure?", "Yes", "No");
			if (answer == false) return;

			vm.DeleteClass();
		}
	}
}