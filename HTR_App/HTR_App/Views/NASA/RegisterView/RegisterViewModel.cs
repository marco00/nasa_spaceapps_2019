﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;
using HTR_App.Services.Interfaces;

namespace HTR_App.Views.NASA.RegisterView
{
    public class RegisterViewModel : HTRAppViewModelBase
    {
	    public RegisterViewModel(INavigationService navigationService, ICRUDTeacherService crudTeacherService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    NewUser = new User();
		    CRUDTeacherService = crudTeacherService;

	    }
		public ICRUDTeacherService CRUDTeacherService { get; set; }
		public User NewUser { get; set; }
		public ICommand RegisterCommand => new RelayCommand(RegisterProc);

	    private void RegisterProc()
	    {
			CRUDTeacherService.AddTeacher(NewUser);
			NavigationService.GoBack();
		    
	    }
	}
}
