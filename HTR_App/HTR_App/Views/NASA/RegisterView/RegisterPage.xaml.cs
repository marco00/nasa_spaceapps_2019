﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.RegisterView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent();
			var vm = App.Locator.RegisterViewModel;
			BindingContext = vm;

		}
	}
}