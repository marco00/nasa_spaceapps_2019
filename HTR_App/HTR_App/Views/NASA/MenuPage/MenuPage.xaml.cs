﻿using HTR_App.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.MenuPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ContentPage
	{
		public MenuPage()
		{
			InitializeComponent();
            var vm = App.Locator.MenuViewModel;
            BindingContext = vm;
        }
		public MenuPage(User user)
		{
			InitializeComponent();
			var vm = App.Locator.MenuViewModel;
			vm.User = user;
			BindingContext = vm;

		}
	}
}