﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Services;
using Xamarin.Forms;

namespace HTR_App.Views.NASA.MenuPage
{
	public class MenuViewModel : HTRAppViewModelBase
	{

		public MenuViewModel(INavigationService navigationService)
		{
			if (navigationService == null) throw new ArgumentNullException("navigationService");
			NavigationService = (NavigationService)navigationService;
		}
		//TODO Move logout button to HomePage and remove MenuPage. MasterDetail page unnecessary for application
		public ICommand LogoutCommand => new RelayCommand(LogoutProc);

		public void LogoutProc()
		{
			NavigationService.NavigateTo(ViewModelLocator.LoginPage,null, true);
			SettingsImplementation.User = null;
			((MasterDetailPage)App.Current.MainPage).IsGestureEnabled = false;
			SettingsImplementation.IsLoggedIn = false;
		}
	}
}
