﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.ProfileView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileView : ContentPage
    {
        private ProfileView_ViewModel vm;
        public ProfileView()
        {
            InitializeComponent();
            vm = App.Locator.ProfileView_ViewModel;
            BindingContext = vm;
        }
    }
}