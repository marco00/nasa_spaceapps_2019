﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.HomeListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeListView : TabbedPage
    {
        private HomeListView_ViewModel vm;
        public HomeListView()
        {
            InitializeComponent();
            vm = App.Locator.HomeListView_ViewModel;
            BindingContext = vm;

        }
        public HomeListView(User user)
        {
            InitializeComponent();
            vm = App.Locator.HomeListView_ViewModel;
            vm.User = user;
            BindingContext = vm;

        }
    }
}