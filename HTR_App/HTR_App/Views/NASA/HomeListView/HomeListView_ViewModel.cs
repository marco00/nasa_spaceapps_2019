﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.Views.NASA.HomeListView
{
    public class HomeListView_ViewModel : HTRAppViewModelBase
    {
        public HomeListView_ViewModel(INavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("NavigationService");
            NavigationService = (NavigationService)navigationService;
        }

    }
}
