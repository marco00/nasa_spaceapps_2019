﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.DisasterListPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DisasterListPage : ContentPage
    {
        private DisasterListPage_ViewModel vm;
        public DisasterListPage(User user)
        {
            InitializeComponent();
            vm = App.Locator.DisasterListPage_ViewModel;
            vm.User = user;
            BindingContext = vm;
        }

        public DisasterListPage()
        {
            InitializeComponent();
            vm = App.Locator.DisasterListPage_ViewModel;
            BindingContext = vm;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vm.GetEventsFromApi();
            
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            vm.NavigateToDisasterDetails();
            App.Locator.DisasterDetailsPage_ViewModel.SelectedEvent = vm.SelectedEvent;
            Task.Run(() => App.Locator.DisasterDetailsPage_ViewModel.GenerateSelectedEventImage());
        }
        
        private void SwipeGestureRecognizer_OnSwipedLeft(object sender, SwipedEventArgs e)
        {
            Console.WriteLine("Not yet implemented");
        }
        private void SwipeGestureRecognizer_OnSwipedRight(object sender, SwipedEventArgs e)
        {
            Console.WriteLine("Not yet implemented");
        }
    }
}