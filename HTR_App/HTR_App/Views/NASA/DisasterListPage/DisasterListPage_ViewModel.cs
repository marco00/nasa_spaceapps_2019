﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Models.NasaApiModels;
using HTR_App.Services;
using NUnit.Framework.Internal.Execution;
using Xamarin.Forms;
using RootObject = HTR_App.Models.RootObject;

namespace HTR_App.Views.NASA.DisasterListPage
{
    public class DisasterListPage_ViewModel : HTRAppViewModelBase
    {
        private bool _isRefreshing;
        private event_ _selectedEvent;
        public ObservableCollection<event_> Events { get; set; } = new ObservableCollection<event_>();

        public event_ SelectedEvent
        {
            get => _selectedEvent;
            set
            {
                _selectedEvent = value;
                RaisePropertyChanged(nameof(SelectedEvent));

            }
        }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set
            {
                _isRefreshing = value; 
                RaisePropertyChanged(nameof(IsRefreshing));
            }
        }

        public EventsService EventsService { get; set; }

        public DisasterListPage_ViewModel(INavigationService navigationService, EventsService eventsService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            NavigationService = (NavigationService)navigationService;
            EventsService = eventsService;
            GetEventsFromApi();
        }
        public void NavigateToDisasterDetails()
        {
            NavigationService.NavigateTo(ViewModelLocator.DisasterDetailsPage, SelectedEvent, false);
        }
        public async void GetEventsFromApi()
        {
            IsRefreshing = true;
            var z = await Task.Run(() => EventsService.Getter());
            foreach (var v in z.events)
            {
                Events.Add(v);
            }

            IsRefreshing = false;
        }


        public ICommand RefreshCommand => new RelayCommand(GetEventsFromApi);
    }
}
