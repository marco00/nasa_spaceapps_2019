﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Android.Net;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Models.NasaApiModels;
using HTR_App.Services;
using Xamarin.Forms;
using Uri = System.Uri;

namespace HTR_App.Views.NASA.DisasterDetailsPage
{
    public class DisasterDetailsPage_ViewModel : HTRAppViewModelBase
    {
        private bool _isRefreshing;
        private float _totalPool;
        private EarthObject _earthObject;
        private event_ _selectedEvent;

        public DisasterDetailsPage_ViewModel(INavigationService navigationService, ContributorService contributorService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            
            NavigationService = (NavigationService)navigationService;
            ContributorService = contributorService;
        }


        public event_ SelectedEvent
        {
            get => _selectedEvent;
            set
            {
                _selectedEvent = value;
                RaisePropertyChanged(nameof(SelectedEvent));

            }
        }

        public EarthObject EarthObject
        {
            get => _earthObject;
            set
            {
                _earthObject = value;
                RaisePropertyChanged(nameof(EarthObject));

            }
        }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set
            {
                _isRefreshing = value; 
                RaisePropertyChanged(nameof(IsRefreshing));
            }
        }

        public float TotalPool
        {
            get => _totalPool;
            set
            {
                _totalPool = value;
                RaisePropertyChanged(nameof(TotalPool));
            }
        }

        public Uri imageplease { get; set; }

        public ContributorService ContributorService { get; set; }
        public ObservableCollection<Contributor> Contributors { get; set; } = new ObservableCollection<Contributor>();
        public ICommand GotoDisasterUpdatesCommand => new RelayCommand(GotoDisasterDetailsProc);


        private void GotoDisasterDetailsProc()
        {
            NavigationService.NavigateTo(ViewModelLocator.DisasterUpdatesPage, SelectedEvent, false);
        }
        public ICommand RefreshCommand => new RelayCommand(LoadContributors);
        public void LoadContributors()
        {
            IsRefreshing = true;
            Contributors.Clear();
            var contributors = ContributorService.GetContributors(SelectedEvent.Id).OrderByDescending(d => d.FundsDonated);
            foreach (var v in contributors)
            {
                Contributors.Add(v);
            }

            TotalPool = Contributors.Sum(d => d.FundsDonated);
            IsRefreshing = false;
        }

        public async void GenerateSelectedEventImage()
        {
            var z = await Task.Run(() => ContributorService.Imager(SelectedEvent));
            EarthObject = z;
        }

    }
}
