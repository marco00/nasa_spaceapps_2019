﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.Models.NasaApiModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.DisasterDetailsPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DisasterDetailsPage : ContentPage
    {
        private DisasterDetailsPage_ViewModel vm;
        public DisasterDetailsPage(event_ selectedEvent)
        {
            InitializeComponent();
            vm = App.Locator.DisasterDetailsPage_ViewModel;
            vm.SelectedEvent = selectedEvent;
            BindingContext = vm;
            Title = selectedEvent.title;
        }

        public DisasterDetailsPage()
        {
            InitializeComponent();
            vm = App.Locator.DisasterDetailsPage_ViewModel;
            BindingContext = vm;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vm.LoadContributors();
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
        }
    }
}