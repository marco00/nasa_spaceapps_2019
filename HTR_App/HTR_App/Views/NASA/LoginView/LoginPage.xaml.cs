﻿using System.Diagnostics;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.LoginView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage()
		{
			InitializeComponent();
			this.BindingContext = App.Locator.LoginViewModel;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			var currentPageKeyString = ServiceLocator.Current
				.GetInstance<INavigationService>()
				.CurrentPageKey;
			Debug.WriteLine("Current page key: " + currentPageKeyString);
		}
	}
}