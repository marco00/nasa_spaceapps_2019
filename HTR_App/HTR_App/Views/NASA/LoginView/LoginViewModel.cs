﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;
using HTR_App.Services.Interfaces;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HTR_App.Views.NASA.LoginView
{
	public class LoginViewModel : HTRAppViewModelBase
	{

		//TODO Teachers and Login page should be removed later. Just assume that only one person will be using the app on the phone for simplicity
		public LoginViewModel(INavigationService navigationService, ILoginService loginService)
		{
			if (navigationService == null) throw new ArgumentNullException("navigationService");

			NavigationService = (NavigationService)navigationService;
			LoginService = loginService;
			//TODO Remove this after testing
			Username = "Angelo";
			Password = "0000";
		}
		public ILoginService LoginService { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public ICommand LoginCommand => new RelayCommand(LoginProc);

		public void LoginProc()
        {
            var random = new Random();
            var age = random.Next(20,50);
            var user = new User()
            {
                FirstName = "Tester",
                LastName = "Tester",
                ContactNumber = random.Next(),
                Address = "Address",
                Age = age,
                Birthdate = DateTime.Now.AddDays(age * 365 * -1),
                Password = Password,
                Username = Username,
                Email = "email@email.com",
                ReTypePassword = Password
            };
			//var user = LoginService.Check(Username, Password);
			if (user != null)
			{
                NavigationService.NavigateTo(ViewModelLocator.DisasterListPage, user, true);
                SettingsImplementation.User = JsonConvert.SerializeObject(user);
                SettingsImplementation.IsLoggedIn = true;
                ((MasterDetailPage)App.Current.MainPage).IsGestureEnabled = true;
                SettingsImplementation.IsLoggedIn = true;
            }
			else
			{
				Application.Current.MainPage.DisplayAlert("Login Failed", "Please try again", "Close");

				Username = "";
				Password = "";
			}
		}

		public ICommand RegisterCommand => new RelayCommand(RegisterProc);

		private void RegisterProc()
		{
			//TODO Implement register function
			NavigationService.NavigateTo(ViewModelLocator.RegisterPage, null , false);
		}

	}
}
