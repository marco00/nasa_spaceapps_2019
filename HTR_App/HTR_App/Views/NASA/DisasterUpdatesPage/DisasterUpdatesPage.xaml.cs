﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models.NasaApiModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views.NASA.DisasterUpdatesPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DisasterUpdatesPage : ContentPage
    {
        private DisasterUpdatesPage_ViewModel vm;
        public DisasterUpdatesPage(event_ selectedEvent)
        {
            InitializeComponent();
            vm = App.Locator.DisasterUpdatesPage_ViewModel;
            vm.SelectedEvent = selectedEvent;
            BindingContext = vm;
            Title = selectedEvent.title;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vm.LoadUpdates();
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
        }
    }
}