﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models.NasaApiModels;
using HTR_App.Services;

namespace HTR_App.Views.NASA.DisasterUpdatesPage
{
    public class DisasterUpdatesPage_ViewModel : HTRAppViewModelBase
    {
        private bool _isRefreshing;

        public DisasterUpdatesPage_ViewModel(INavigationService navigationService, UpdateService updateService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");

            NavigationService = (NavigationService)navigationService;
            UpdateService = updateService;
        }
        public event_ SelectedEvent { get; set; }
        public UpdateService UpdateService { get; set; }
        public Update SelectedUpdate { get; set; }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set
            {
                _isRefreshing = value;
                RaisePropertyChanged(nameof(IsRefreshing));
            }
        }

        public ObservableCollection<Update> Updates { get; set; } = new ObservableCollection<Update>();
        public ICommand RefreshCommand => new RelayCommand(LoadUpdates);

        public void LoadUpdates()
        {
            IsRefreshing = true;
            var updates = UpdateService.GetUpdates(SelectedEvent.Id);
            Updates.Clear();
            foreach (var v in updates)
            {
                Updates.Add(v);
            }

            IsRefreshing = false;
        }
    }
}
