﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Views;
using HTR_App.Models;
using HTR_App.Services;
using HTR_App.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTR_App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		private HomePageViewModel vm;
		
		public HomePage(User user)
		{
			InitializeComponent();
			vm = App.Locator.HomePageViewModel;
			vm.User = user;
			BindingContext = vm;

		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			var currentPageKeyString = ServiceLocator.Current
				.GetInstance<INavigationService>()
				.CurrentPageKey;
			Debug.WriteLine("Current page key: " + currentPageKeyString);
			vm.LoadClasses(vm.User);
		}

		private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			
			vm.GotoClassDetailsProc();
		}
	}
}