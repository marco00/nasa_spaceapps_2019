﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using GalaSoft.MvvmLight;
using HTR_App.Models;
using HTR_App.Services;
using Xamarin.Forms;

namespace HTR_App.Helpers
{

	public class HTRAppViewModelBase : ObservableObject
    {
	    public NavigationService NavigationService { get; set; }
		public User User { get; set; }

    }
}
