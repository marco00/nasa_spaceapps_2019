﻿using System;
using System.Collections.Generic;
using System.Text;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using HTR_App.Models;
using HTR_App.Services;
using HTR_App.Services.Interfaces;
using HTR_App.ViewModels;
using HTR_App.Views.NASA.DisasterDetailsPage;
using HTR_App.Views.NASA.DisasterListPage;
using HTR_App.Views.NASA.DisasterUpdatesPage;
using HTR_App.Views.NASA.HomeListView;
using HTR_App.Views.NASA.LoginView;
using HTR_App.Views.NASA.MenuPage;
using HTR_App.Views.NASA.ProfileView;
using HTR_App.Views.NASA.RegisterView;

namespace HTR_App.Helpers
{

	public class ViewModelLocator
	{
		public const string LoginPage = "LoginPage";
		public const string HomePage = "HomePage";
		public const string MenuPage = "MenuPage";
		public const string TestDetailsPage = "TestDetailsPage";
		public const string ClassDetailsPage = "ClassDetailsPage";
		public const string AddNewSubjectPage = "AddNewClassPage";
		public const string AddNewTestPage = "AddNewTestPage";
		public const string UpdateSubjectPage = "UpdateSubjectPage";
		public const string StudentsEnrolledView = "StudentsEnrolledView";
		public const string AddNewStudentPage = "AddNewStudentPage";
		public const string UpdateStudentPage = "UpdateStudentPage";
		public const string StudentDetailsPage = "StudentDetailsPage";
		public const string UpdateTestScorePage = "UpdateTestScorePage";
		public const string RegisterPage = "RegisterPage";
		public const string AddNewTestScorePage = "AddNewTestScorePage";
        public const string DisasterDetailsPage = "DisasterDetailsPage";
        public const string DisasterListPage = "DisasterListPage";
        public const string DisasterUpdatesPage = "DisasterUpdatesPage";
        public const string ProfilePage = "ProfilePage";
        public const string HomeListView = "HomeListView";


        public bool IsTestMode { get; set; }

		public ViewModelLocator()
		{
			ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            //NASA
            SimpleIoc.Default.Register<DisasterDetailsPage_ViewModel>();
            SimpleIoc.Default.Register<DisasterListPage_ViewModel>();
            SimpleIoc.Default.Register<DisasterUpdatesPage_ViewModel>();
            SimpleIoc.Default.Register<ProfileView_ViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<MenuViewModel>();
            SimpleIoc.Default.Register<RegisterViewModel>();


            SimpleIoc.Default.Register<HomePageViewModel>();
			SimpleIoc.Default.Register<TestDetailsViewModel>();
			SimpleIoc.Default.Register<ClassDetailsViewModel>();
			SimpleIoc.Default.Register<AddClassViewModel>();
			SimpleIoc.Default.Register<AddStudentViewModel>();
			SimpleIoc.Default.Register<AddTestViewModel>();
			SimpleIoc.Default.Register<StudentDetailsViewModel>();
			SimpleIoc.Default.Register<StudentsListViewModel>();
			SimpleIoc.Default.Register<UpdateClassViewModel>();
			SimpleIoc.Default.Register<UpdateStudentViewModel>();
			SimpleIoc.Default.Register<UpdateTestScoreViewModel>();
			SimpleIoc.Default.Register<AddTestScoreViewModel>();
			SimpleIoc.Default.Register<HomeListView_ViewModel>();



            //add if statement here when using mock service

            //for first part of project mock services are not required
            SimpleIoc.Default.Register<ILoginService, MockLoginService>();
            SimpleIoc.Default.Register<ICRUDClassService, CRUDClassService>();
            SimpleIoc.Default.Register<ICRUDTestService, CRUDTestService>();
            SimpleIoc.Default.Register<ICRUDStudentService, CRUDStudentService>();
            SimpleIoc.Default.Register<ICRUDTestScoreService, CRUDTestScoreService>();
            SimpleIoc.Default.Register<ICRUDTeacherService, CRUDTeacherService>();
            SimpleIoc.Default.Register<EventsService, EventsService>();
            SimpleIoc.Default.Register<UpdateService, UpdateService>();
            SimpleIoc.Default.Register<ContributorService, ContributorService>();



        }

		public LoginViewModel LoginViewModel => ServiceLocator.Current.GetInstance<LoginViewModel>();
		public HomePageViewModel HomePageViewModel => ServiceLocator.Current.GetInstance<HomePageViewModel>();
		public MenuViewModel MenuViewModel => ServiceLocator.Current.GetInstance<MenuViewModel>();
		public TestDetailsViewModel TestDetailsViewModel => ServiceLocator.Current.GetInstance<TestDetailsViewModel>();
		public ClassDetailsViewModel ClassDetailsViewModel => ServiceLocator.Current.GetInstance<ClassDetailsViewModel>();
		public AddClassViewModel AddClassViewModel => ServiceLocator.Current.GetInstance<AddClassViewModel>();
		public AddStudentViewModel AddStudentViewModel => ServiceLocator.Current.GetInstance<AddStudentViewModel>();
		public AddTestViewModel AddTestViewModel => ServiceLocator.Current.GetInstance<AddTestViewModel>();
		public StudentDetailsViewModel StudentDetailsViewModel => ServiceLocator.Current.GetInstance<StudentDetailsViewModel>();
		public StudentsListViewModel StudentsListViewModel => ServiceLocator.Current.GetInstance<StudentsListViewModel>();
		public UpdateClassViewModel UpdateClassViewModel => ServiceLocator.Current.GetInstance<UpdateClassViewModel>();
		public UpdateStudentViewModel UpdateStudentViewModel => ServiceLocator.Current.GetInstance<UpdateStudentViewModel>();
		public UpdateTestScoreViewModel UpdateTestScoreViewModel => ServiceLocator.Current.GetInstance<UpdateTestScoreViewModel>();
		public RegisterViewModel RegisterViewModel => ServiceLocator.Current.GetInstance<RegisterViewModel>();
		public AddTestScoreViewModel AddTestScoreViewModel => ServiceLocator.Current.GetInstance<AddTestScoreViewModel>();


        //nasa
		public DisasterListPage_ViewModel DisasterListPage_ViewModel => ServiceLocator.Current.GetInstance<DisasterListPage_ViewModel>();
		public DisasterDetailsPage_ViewModel DisasterDetailsPage_ViewModel => ServiceLocator.Current.GetInstance<DisasterDetailsPage_ViewModel>();
		public DisasterUpdatesPage_ViewModel DisasterUpdatesPage_ViewModel => ServiceLocator.Current.GetInstance<DisasterUpdatesPage_ViewModel>();
		public ProfileView_ViewModel ProfileView_ViewModel => ServiceLocator.Current.GetInstance<ProfileView_ViewModel>();
		public HomeListView_ViewModel HomeListView_ViewModel => ServiceLocator.Current.GetInstance<HomeListView_ViewModel>();
    }
}
