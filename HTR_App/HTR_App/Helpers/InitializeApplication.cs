﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using HTR_App.Models;
using HTR_App.Services;
using HTR_App.Views;
using HTR_App.Views.NASA.DisasterDetailsPage;
using HTR_App.Views.NASA.DisasterListPage;
using HTR_App.Views.NASA.DisasterUpdatesPage;
using HTR_App.Views.NASA.HomeListView;
using HTR_App.Views.NASA.LoginView;
using HTR_App.Views.NASA.MenuPage;
using HTR_App.Views.NASA.ProfileView;
using HTR_App.Views.NASA.RegisterView;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;
using Xamarin.Forms;

namespace HTR_App.Helpers
{
	public class InitializeApplication
	{
		public NavigationService navigationService { get; }
		public InitializeApplication()
		{
			navigationService = new NavigationService();
			SimpleIoc.Default.Reset();
			SetPages();
			SimpleIoc.Default.Register<INavigationService>(() => navigationService);
            //InitializeLocalDatabase();
            SettingsImplementation.IsLoggedIn = false;


        }

		public void InitializeLocalDatabase()
		{
			//expression<func<t,bool> condition
			var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "HTRAppDatabase.db3");
			User user;
			Student student;
			Subject dummySubject;
			Test test;
			TestScore score;
			using (var conn = new SQLiteConnection(path))
			{

				//DeleteDummyData(conn);
				conn.CreateTable<Class_Student>();
				conn.CreateTable<Test_Student>();
				conn.CreateTable<Teacher_Student>();
				conn.CreateTable<User>();
				conn.CreateTable<Student>();
				conn.CreateTable<Subject>();
				conn.CreateTable<Test>();
				conn.CreateTable<TestScore>();
				//conn.CreateTable<Class_Student>();
				//conn.CreateTable<Test_Student>();
				//conn.CreateTable<Teacher_Student>();

				//Dummy data
				SetDummyData(conn);

				//For testing
				var classes = conn.Table<Subject>();
				var list = classes.ToList();

				var students = conn.Table<Student>();
				var list2 = students.ToList();

				var tests = conn.Table<Test>();
				var list3 = tests.ToList();

				var teachers = conn.Table<User>();
				var list4 = teachers.ToList();

				var testScores = conn.Table<TestScore>();
				var list5 = testScores.ToList();

			}

		}

		private void DeleteDummyData(SQLiteConnection conn)
		{
			//conn.DeleteAll<User>();
			//conn.DeleteAll<Test>();
			//conn.DeleteAll<SelectedSubject>();
			conn.DeleteAll<TestScore>();
			//conn.DeleteAll<Student>();
			//conn.DeleteAll<Class_Student>();
			//conn.DeleteAll<Teacher_Student>();
		}
		private void SetDummyData(SQLiteConnection conn)
		{
			conn.Insert(new User() {Username = "0", Password = "0"});
		}


		public void SetPages()
		{
			navigationService.Configure(ViewModelLocator.TestDetailsPage, typeof(TestDetailsPage));
			navigationService.Configure(ViewModelLocator.AddNewSubjectPage, typeof(AddNewClassPage));
			navigationService.Configure(ViewModelLocator.AddNewTestPage, typeof(AddNewTestPage));
			navigationService.Configure(ViewModelLocator.ClassDetailsPage, typeof(ClassDetailsPage)); 
			navigationService.Configure(ViewModelLocator.UpdateSubjectPage, typeof(UpdateSubjectPage));
			navigationService.Configure(ViewModelLocator.StudentsEnrolledView, typeof(StudentsEnrolledView));
			navigationService.Configure(ViewModelLocator.AddNewStudentPage, typeof(AddNewStudentPage));
			navigationService.Configure(ViewModelLocator.UpdateStudentPage, typeof(UpdateStudentPage));
			navigationService.Configure(ViewModelLocator.StudentDetailsPage, typeof(StudentDetailsPage));
			navigationService.Configure(ViewModelLocator.UpdateTestScorePage, typeof(UpdateTestScorePage));

            //NASA
			navigationService.Configure(ViewModelLocator.DisasterDetailsPage, typeof(DisasterDetailsPage));
			navigationService.Configure(ViewModelLocator.DisasterListPage, typeof(DisasterListPage));
			navigationService.Configure(ViewModelLocator.DisasterUpdatesPage, typeof(DisasterUpdatesPage));
			navigationService.Configure(ViewModelLocator.ProfilePage, typeof(ProfileView));
            navigationService.Configure(ViewModelLocator.HomePage, typeof(HomePage));
			navigationService.Configure(ViewModelLocator.MenuPage, typeof(MenuPage));
			navigationService.Configure(ViewModelLocator.LoginPage, typeof(LoginPage));
            navigationService.Configure(ViewModelLocator.RegisterPage, typeof(RegisterPage));
            navigationService.Configure(ViewModelLocator.HomeListView, typeof(HomeListView));

        }

        public MasterDetailPage SetMasterDetailMainPage()
		{
			bool isGestureEnabled;
			var navigationPage = new NavigationPage();
			var user = new User();
			if (SettingsImplementation.IsLoggedIn)
			{
				var userJsonString = JToken.Parse(SettingsImplementation.User).ToString();
				user = JsonConvert.DeserializeObject<User>(userJsonString);
				navigationPage = new NavigationPage(new HomePage(user));
				isGestureEnabled = true;
			}
			else
			{
				navigationPage = new NavigationPage(new LoginPage());
				isGestureEnabled = false;
			}


			var masterDetailPage = new MasterDetailPage
			{
				Detail = navigationPage,
				Master = new MenuPage(user) { Title = "Menu" }
			};
			navigationService.Initialize(navigationPage);
			masterDetailPage.IsGestureEnabled = isGestureEnabled;

			return masterDetailPage;

		}

        public MasterDetailPage NASA_GenerateMasterDetailPage()
        {
            bool isGestureEnabled;
            var user = new User();

            var masterDetailsPage = new MasterDetailPage();
            var navigationPage = new NavigationPage();
            if (SettingsImplementation.IsLoggedIn)
            {
                var userJsonString = JToken.Parse(SettingsImplementation.User).ToString();
                user = JsonConvert.DeserializeObject<User>(userJsonString);

                navigationPage = new NavigationPage(new DisasterListPage(user));
                isGestureEnabled = true;

                
            }
            else
            {

                navigationPage = new NavigationPage(new LoginPage());
                isGestureEnabled = false;
            }
            masterDetailsPage = new MasterDetailPage
            {

                Detail = navigationPage,
                Master = new MenuPage(user) { Title = "Menu" }
            };
            navigationService.Initialize(navigationPage);
            masterDetailsPage.IsGestureEnabled = isGestureEnabled;

            return masterDetailsPage;
        }
    }
}
