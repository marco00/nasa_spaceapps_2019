﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models;
using HTR_App.Models.NasaApiModels;

namespace HTR_App.Services
{
    public class ContributorService
    {
        public List<Contributor> GetContributors(int eventId)
        {
            var random = new Random();
            var contributors = new List<Contributor>();
            for(int x = 0 ; x < 20; x++)
            {
                var age = random.Next(20, 50);
                contributors.Add(new Contributor()
                {
                    FirstName = "Tester",
                    LastName = "Tester",
                    ContactNumber = random.Next(),
                    Address = "Address",
                    Age = age,
                    Birthdate = DateTime.Now.AddDays(age * 365 * -1),
                    Email = "email@email.com",
                    FundsDonated = random.Next(100,20000)
                });
            }
            return contributors;
        }

        public async Task<EarthObject> GetAsyncEarthObject(event_ SelectedEvent)
        {
            ApiHelper.InitializeClient();
            var latitude = SelectedEvent.geometries.FirstOrDefault().coordinates.FirstOrDefault();
            var longitude = SelectedEvent.geometries.FirstOrDefault().coordinates.LastOrDefault();
            string templateurl =
                string.Format(
                    @"https://api.nasa.gov/planetary/earth/imagery/?lat={0}&lon={1}&api_key=21ZTyMt5zcoQugv8e81myVDun9Mdxn1s6a7CfiCv", latitude, longitude);
            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(templateurl))
            {
                if (response.IsSuccessStatusCode)
                {
                    var foo = await response.Content.ReadAsAsync<EarthObject>();
                    return foo;
                }
                else
                {
                    var foo = new EarthObject();
                    foo.url = "the API currently has no image on the given Latitude and Longitude Spatial Coordinates.";
                    return foo;
                }
            }
        }

        public async Task<EarthObject> Imager(event_ SelectedEvent)
        {
            var Result = GetAsyncEarthObject(SelectedEvent).Result;
            return Result;
        }

    }
}
