﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models.NasaApiModels;

namespace HTR_App.Services
{
    public class UpdateService
    {
        public List<Update> Updates = new List<Update>();
        public List<Update> GetUpdates(int eventId)
        {
            //mock update service
            var random = new Random();
            if (Updates.Count == 0)
            {
                for (int x = 0; x < 10; x++)
                {
                    Updates.Add(new Update()
                    {
                        Description = $"Description {random.Next()}",
                        Title = $"title {random.Next()}",
                        NumberOfLikes = random.Next(10,50)
                    });
                }

                return Updates;
            }
            else
            {
                foreach (var v in Updates)
                {
                    v.NumberOfLikes += random.Next(5);
                }

                return Updates;
            }
        }
    }
}
