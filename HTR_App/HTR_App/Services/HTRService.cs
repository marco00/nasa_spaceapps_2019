﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Newtonsoft.Json.Linq;

namespace HTR_App.Services
{
	public class HTRService
    {
	    const string subscriptionKey = "1455e47165be4dd694cc2afea2368ed1";

	    // You must use the same Azure region in your REST API method as you used to
	    // get your subscription keys. For example, if you got your subscription keys
	    // from the West US region, replace "westcentralus" in the URL
	    // below with "westus".
	    //
	    // Free trial subscription keys are generated in the "westus" region.
	    // If you use a free trial subscription key, you shouldn't need to change
	    // this region.
	    const string uriBase =
			"https://southeastasia.api.cognitive.microsoft.com/";

	    private TextRecognitionMode RecognitionMode = TextRecognitionMode.Handwritten;
	    public async Task<TextOperationResult> UploadAndRecognizeImageAsync(string imageFilePath)
	    {
		    using (Stream imageFileStream = File.OpenRead(imageFilePath))
		    {
			    return await RecognizeAsync(
				    async (ComputerVisionClient client) => await client.RecognizeTextInStreamAsync(imageFileStream, RecognitionMode),
				    headers => headers.OperationLocation);
		    }
	    }
		private async Task<TextOperationResult> RecognizeAsync<T>(Func<ComputerVisionClient, Task<T>> GetHeadersAsyncFunc, Func<T, string> GetOperationUrlFunc) where T : new()
		{
			// -----------------------------------------------------------------------
			// KEY SAMPLE CODE STARTS HERE
			// -----------------------------------------------------------------------
			var result = default(TextOperationResult);
			var Credentials = new ApiKeyServiceClientCredentials(subscriptionKey);
			//
			// Create Cognitive Services Vision API Service client.
			//
			using (var client = new ComputerVisionClient(Credentials) { Endpoint = uriBase })
			{

				try
				{

					T recognizeHeaders = await GetHeadersAsyncFunc(client);
					string operationUrl = GetOperationUrlFunc(recognizeHeaders);
					string operationId = operationUrl.Substring(operationUrl.LastIndexOf('/') + 1);
	
					result = await client.GetTextOperationResultAsync(operationId);

					for (int attempt = 1; attempt <= 3; attempt++)
					{
						if (result.Status == TextOperationStatusCodes.Failed || result.Status == TextOperationStatusCodes.Succeeded)
						{
							break;
						}

						await Task.Delay(TimeSpan.FromSeconds(3));

						result = await client.GetTextOperationResultAsync(operationId);
					}

				}
				catch (Exception ex)
				{
					result = new TextOperationResult() { Status = TextOperationStatusCodes.Failed };
				}
				return result;
			}

			// -----------------------------------------------------------------------
			// KEY SAMPLE CODE ENDS HERE
			// -----------------------------------------------------------------------
		}






	}

}
