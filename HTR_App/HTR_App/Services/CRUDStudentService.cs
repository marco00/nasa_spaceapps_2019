﻿using System.Collections.Generic;
using System.Linq;
using HTR_App.Models;
using HTR_App.Repositories;
using HTR_App.Repositories.LocalRepository;

namespace HTR_App.Services
{
	public class CRUDStudentService : ICRUDStudentService
	{
		private IRepository repository;

		public CRUDStudentService()
		{
			repository = new LocalRepository();
			
		}
		public List<Student> GetStudentsEnrolledInClass(Subject subject)
		{
			var student_subject_tables = repository.Class_Student.GetRange(c => c.ClassId == subject.Id);
			var student = new List<Student>();
			foreach (var v in student_subject_tables)
			{
				student.Add(repository.Student.Get(c => c.Id == v.StudentId));
			}
			var students =  student_subject_tables.Select(v => repository.Student.Get(c => c.Id == v.StudentId)).ToList();
			return student;
		}

		public Student GetStudentFromId(int id)
		{
			return repository.Student.Get(c => c.Id == id);
		}
		public void AddStudent(Student student, Subject subject, User user)
		{
			repository.Student.Add(student);
			repository.Class_Student.Add(new Class_Student() { ClassId = subject.Id, StudentId = student.Id });
			repository.Teacher_Student.Add(new Teacher_Student() { TeacherId = user.Id, StudentId = student.Id });
		}
		public void DeleteStudent(Student student)
		{
			var students = repository.Student.GetAllContents();
			var student_class = repository.Class_Student.GetAllContents();
			var teacher_student = repository.Teacher_Student.GetAllContents();
			repository.Class_Student.Delete(c => c.StudentId == student.Id);
			repository.Teacher_Student.Delete(c => c.StudentId == student.Id);
			repository.Student.Delete(c => c.Id == student.Id);
			
			
		}
		public void EditStudent(Student oldStudent, Student newStudent)
		{
			repository.Student.Update(newStudent);

		}


	}
}