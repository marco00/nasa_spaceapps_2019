﻿using System;
using System.Collections.Generic;
using System.Text;
using HTR_App.Models;
using HTR_App.Repositories;
using HTR_App.Repositories.LocalRepository;

namespace HTR_App.Services
{
	public class CRUDClassService : ICRUDClassService
    {
	    private IRepository repository;

	    public CRUDClassService()
	    {
		    repository = new LocalRepository();
	    }
	    public void AddClass(Subject subject)
	    {
		    if (repository.Subject.Get(c => c.Code == subject.Code || c.Name == subject.Name) == null)
		    {
				repository.Subject.Add(subject);
			}
		}

	    public List<Subject> GetSubjects(User user)
	    {
			return repository.Subject.GetRange(c => c.TeacherId == user.Id);

	    }

	    public Subject GetSubjectFromId(int id)
	    {
		    return repository.Subject.Get(c => c.Id == id);
	    }

	    public void EditSubject(Subject oldSubject, Subject newSubject)
	    {
		    repository.Subject.Update(newSubject);
	    }

	    public void DeleteSubject(Subject subject)
	    {
		    repository.Subject.Delete(c => c.Id == subject.Id);
		    var class_students = repository.Class_Student.GetRange(c => c.ClassId == subject.Id);
			foreach (var v in class_students)
		    {
			    //remove all students enrolled in the class
				repository.Student.Delete(c => c.Id == v.StudentId);
			    //remove all test scores related to this class
			    repository.TestScore.DeleteMultiple(c => c.StudentId == v.StudentId);
				//remove all teacher_student classes with the students enrolled in this class
				repository.Teacher_Student.DeleteMultiple(c => c.StudentId == v.StudentId);
			    //remove all test_student tables related to this class
				repository.Test_Student.DeleteMultiple(c =>c.StudentId == v.StudentId);
			}

			//remove all tests taken in this class
			repository.Test.DeleteMultiple(c => c.SubjectId == subject.Id);
			//remove all instances of class_student with this subject data
			repository.Class_Student.DeleteMultiple(c => c.ClassId == subject.Id);

			var a = repository.Subject.GetAllContents();
		    var b = repository.Class_Student.GetAllContents();
		    var cc = repository.Student.GetAllContents();
		    var d = repository.Teacher.GetAllContents();
		    var e = repository.Teacher_Student.GetAllContents();
		    var f = repository.Test.GetAllContents();
		    var g = repository.TestScore.GetAllContents();
		    var ha = repository.TestScore.GetAllContents();
	    }
	}
}
