﻿using System.Collections.Generic;
using HTR_App.Models;
using HTR_App.Repositories;
using HTR_App.Repositories.LocalRepository;
using Xamarin.Forms;

namespace HTR_App.Services
{
	public class CRUDTestService : ICRUDTestService{
		private IRepository repository;

		public CRUDTestService()
		{
			repository = new LocalRepository();
		}


		public void AddTest(Test test)
		{
			if (repository.Test.Get(c => c.Name == test.Name) == null)
			{
				repository.Test.Add(test);
			}
			else
			{
				Application.Current.MainPage.DisplayAlert("Test name already taken", "Please try another name", "Close");

			}
			;
		}

		public List<Test> GetTests(Subject subject)
		{
			return repository.Test.GetRange(c => c.SubjectId == subject.Id);

		}

		public Test GetTestFromId(int id)
		{
			return repository.Test.Get(c => c.Id == id);
		}

		public void EditTest(Test oldTest, Test newTest)
		{
			repository.Test.Update(newTest);

		}

		public void DeleteTest(Test test)
		{
			repository.Test.Delete(c => c.Id == test.Id);
		}
	}
}