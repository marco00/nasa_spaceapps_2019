﻿using System;
using System.Collections.Generic;
using System.Text;
using HTR_App.Models;
using HTR_App.Repositories;
using HTR_App.Repositories.LocalRepository;
using HTR_App.Services.Interfaces;

namespace HTR_App.Services
{
    public class CRUDTeacherService : ICRUDTeacherService
    {
	    private IRepository repository;

	    public CRUDTeacherService()
	    {
		    repository = new LocalRepository();

	    }
		public void AddTeacher(User user)
	    {
			if (repository.Teacher.Get(c => c.LastName == user.LastName && c.FirstName == user.FirstName) == null)
			{
				repository.Teacher.Add(user);
			}
		}

	    public List<User> GetTeachers()
	    {
		    return repository.Teacher.GetAllContents();
	    }

	    public User GetTeacherFromId(int id)
	    {
			return repository.Teacher.Get(c => c.Id == id);

		}

		public void EditTeacher(User newUser)
	    {
			repository.Teacher.Update(newUser);

		}

		public void DeleteTeacher(User user)
	    {
			repository.Teacher.Delete(c => c.Id == user.Id);

		}
	}
}
