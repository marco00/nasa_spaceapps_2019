﻿using System;
using System.Collections.Generic;
using System.Text;
using HTR_App.Helpers;

namespace HTR_App.Services
{
    public class CorrectiveAlgorithms
    {
	    public float HowLikelyToBeAValidScore(string input)
	    {
		    float probability = 0;

		    if (input.Contains("/"))
		    {
			    probability += 0.25f;
			    var list = input.Split('/');
			    if (list.Length == 2)
			    {
				    list[0] = list[0].Trim();
				    list[1] = list[1].Trim();

				    var num1 = 0.0;
				    var num2 = 0.0;
				    if (double.TryParse(list[0], out num1)) probability += .25f;
				    if (double.TryParse(list[1], out num2)) probability += .25f;
				    if (num1 <= num2) probability += 0.25f;
			    }
		    }
		    return probability;

	    }

	    public List<string> AutoComplete(string input, List<string> dictionary, out bool clearSuggestedNames)
	    {
		    var output = new List<string>();
		    var totalScore = 0.0;
		    foreach (var v in dictionary)
		    {
			    var score = 1.0 - JaroWinklerDistance.distance(input, v);
			    //threshold is based on https://medium.com/@appaloosastore/string-similarity-algorithms-compared-3f7b4d12f0ff
			    if (score > 0.863)
			    {
				    output.Add(v);
			    }
			    totalScore += score;
		    }
		    if (totalScore / output.Count > 0.8)
		    {
			    clearSuggestedNames = true;
		    }
		    else
		    {
			    clearSuggestedNames = false;
		    }

		    return output;
	    }

		/// <summary>
		/// Return 1 for score and 0 for name
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
	    public int IsNameOrScore(string input)
		{
			int num;
			if (int.TryParse(input, out num))
			{
				return 2;
			}
		    var isAScoreProbability = HowLikelyToBeAValidScore(input);
		    if (isAScoreProbability < 0.25)
		    {
			    //is most likely to be a name
			    return 0;
		    }
		    return 1;
	    }
	}
}
