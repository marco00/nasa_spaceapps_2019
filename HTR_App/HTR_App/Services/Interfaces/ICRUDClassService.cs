﻿using System.Collections.Generic;
using HTR_App.Models;

namespace HTR_App.Services
{
	public interface ICRUDClassService
	{
		void AddClass(Subject subject);
		List<Subject> GetSubjects(User user);
		Subject GetSubjectFromId(int id);
		void EditSubject(Subject oldSubject, Subject newSubject);
		void DeleteSubject(Subject subject);

	}
}