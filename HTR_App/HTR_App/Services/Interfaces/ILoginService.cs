﻿using HTR_App.Models;

namespace HTR_App.Services.Interfaces
{
	public interface ILoginService
	{
		User Check(string username, string password);

	}
}
