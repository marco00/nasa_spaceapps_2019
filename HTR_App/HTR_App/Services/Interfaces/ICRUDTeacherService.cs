﻿using System;
using System.Collections.Generic;
using System.Text;
using HTR_App.Models;

namespace HTR_App.Services.Interfaces
{
    public interface ICRUDTeacherService
    {
	    void AddTeacher(User user);
	    List<User> GetTeachers();
	    User GetTeacherFromId(int id);
	    void EditTeacher(User newUser);
	    void DeleteTeacher(User user);

	}
}
