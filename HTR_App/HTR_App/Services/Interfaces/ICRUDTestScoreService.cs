﻿using System.Collections.Generic;
using HTR_App.Models;

namespace HTR_App.Services
{
	public interface ICRUDTestScoreService
	{
		void AddTestScore(TestScore score);
		List<TestScore> GetTestScores(Test test);
		void EditTestScore(TestScore oldTestScore, TestScore newTestScore);
		void DeleteTestScore(TestScore testScore);
	}
}