﻿using System.Collections.Generic;
using HTR_App.Models;

namespace HTR_App.Services
{
	public interface ICRUDTestService
	{
		void AddTest(Test test);
		List<Test> GetTests(Subject subject);
		Test GetTestFromId(int id);
		void EditTest(Test oldTest, Test newTest);
		void DeleteTest(Test test);

	}
}
