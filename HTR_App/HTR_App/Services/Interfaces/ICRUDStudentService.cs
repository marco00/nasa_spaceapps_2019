﻿using System.Collections.Generic;
using HTR_App.Models;

namespace HTR_App.Services
{
	public interface ICRUDStudentService
	{
		void EditStudent(Student oldStudent, Student newStudent);
		void DeleteStudent(Student student);
		List<Student> GetStudentsEnrolledInClass(Subject subject);
		Student GetStudentFromId(int id);
		void AddStudent(Student student, Subject subject, User user);

	}
}
