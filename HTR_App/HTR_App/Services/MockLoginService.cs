﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using HTR_App.Models;
using HTR_App.Repositories;
using HTR_App.Repositories.LocalRepository;
using HTR_App.Services.Interfaces;

namespace HTR_App.Services
{
	public class MockLoginService : ILoginService
	{
		private IRepository _repository;
		public MockLoginService()
		{
			_repository = new LocalRepository();
		}

		public User Check(string username, string password)
		{
			var teacher = _repository.Teacher.Get(c => c.Username == username && c.Password == password);
			return teacher;
		}


	}
}
