﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HTR_App.Models.NasaApiModels;

namespace HTR_App.Services
{
    public class EventsService
    {
        
        //public List<event_> GetEvents()
        //{
        //}
        
        public async Task<RootObject> GetAsyncRootObject()
        {
            ApiHelper.InitializeClient();
            string url = $"https://eonet.sci.gsfc.nasa.gov/api/v2.1/events?limit=141";
            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {

                    var foo = await response.Content.ReadAsAsync<RootObject>();
                    return foo;
                }
                else
                {
                    throw new Exception("No Internet");
                }
            }
        }

        public async Task<RootObject> Getter()
        {
            var Result = GetAsyncRootObject().Result;
            return Result;
        }


        //dont error lord


    }
}
