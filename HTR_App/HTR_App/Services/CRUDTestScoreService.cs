﻿using System.Collections.Generic;
using HTR_App.Models;
using HTR_App.Repositories;
using HTR_App.Repositories.LocalRepository;

namespace HTR_App.Services
{
	public class CRUDTestScoreService : ICRUDTestScoreService
	{
		public IRepository repository;

		public CRUDTestScoreService()
		{
			repository = new LocalRepository();

		}
		public void AddTestScore(TestScore score)
		{
			var scores = repository.TestScore.GetAllContents();
			if (repository.TestScore.Get(c => c.TestId == score.TestId && c.StudentId == score.StudentId) == null)
			{
				repository.TestScore.Add(score);
			}
		}

		public List<TestScore> GetTestScores(Test test)
		{
			return repository.TestScore.GetRange(c => c.TestId == test.Id);
		}

		public void EditTestScore(TestScore oldTestScore, TestScore newTestScore)
		{
			repository.TestScore.Update(newTestScore);
		}

		public void DeleteTestScore(TestScore testScore)
		{
			repository.TestScore.Delete(c => c.TestId == testScore.TestId && c.StudentId == testScore.StudentId);
		}
	}
}