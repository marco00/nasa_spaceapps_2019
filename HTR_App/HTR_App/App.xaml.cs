﻿using System;
using HTR_App.Helpers;
using HTR_App.Views;
using HTR_App.Views.NASA.LoginView;
using HTR_App.Views.NASA.MainMasterDetailPage;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace HTR_App
{
	public partial class App : Application
	{
		public static ViewModelLocator Locator;
		private readonly InitializeApplication _initApplication;
		public App()
		{
			InitializeComponent();
			if (_initApplication == null)
			{
				_initApplication = new InitializeApplication();
			}
			Locator = new ViewModelLocator();

            ////NavigationService  = _initApplication.nav
            //var firstPage = new NavigationPage(new LoginPage());
            //_initApplication.navigationService.Initialize(firstPage);
            //MainPage = firstPage;
            MainPage = _initApplication.NASA_GenerateMasterDetailPage();
            
        }

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
