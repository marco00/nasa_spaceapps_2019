﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class AddTestScoreViewModel : HTRAppViewModelBase
    {
	    public TestScore TestScore
	    {
		    get { return _testScore; }
		    set
		    {
			    _testScore = value;
			    RaisePropertyChanged(nameof(TestScore));
		    }
	    }

	    public AddTestScoreViewModel(INavigationService navigationService, ICRUDTestScoreService iCRUDTestScoreService, ICRUDStudentService iCrudStudentService, ICRUDClassService iCrudClassService)
	    {
		    if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;

		    CRUDTestScoreService = iCRUDTestScoreService;
		    CRUDClassService = iCrudClassService;
		    CRUDStudentService = iCrudStudentService;
		}

	    public void InitializeViewModel()
	    {
			SelectedStudent = new Student();
		    SelectedSuggestion = new Student();
		    ShowSuggestionsListIsVisible = true;

		}
	    public void LoadStudents()
	    {
		    FullListOfStudents.Clear();
		    var subject = CRUDClassService.GetSubjectFromId(SelectedTest.SubjectId);

			var students = CRUDStudentService.GetStudentsEnrolledInClass(subject);
		    foreach (var v in students)
		    {
			    FullListOfStudents.Add(v);
		    }
	    }

	    private ICRUDStudentService CRUDStudentService;
	    private ICRUDClassService CRUDClassService;
		private ICRUDTestScoreService CRUDTestScoreService;
	    public Student SelectedSuggestion { get; set; }
	    public Student SelectedStudent { get; set; }
		public ObservableCollection<Student> SuggestedStudents { get; set; } = new ObservableCollection<Student>();
		public ObservableCollection<Student> FullListOfStudents { get; set; } = new ObservableCollection<Student>();
		public ObservableCollection<Student> StudentList { get; set; } = new ObservableCollection<Student>();
	    private bool _showSuggestionsListIsVisible;
	    private TestScore _testScore;

	    public bool ShowSuggestionsListIsVisible
	    {
		    get { return _showSuggestionsListIsVisible; }
		    set
		    {
			    _showSuggestionsListIsVisible = value;
			    RaisePropertyChanged(nameof(ShowSuggestionsListIsVisible));
		    }
	    }


	    public ICommand ReplaceSuggestionsListCommand => new RelayCommand(ReplaceSuggestionsList);

	    private void ReplaceSuggestionsList()
	    {
			LoadStudents();
		    StudentList.Clear();
		    foreach (var v in FullListOfStudents)
		    {
			    StudentList.Add(v);
		    }
		    ShowSuggestionsListIsVisible = false;
	    }

	    public ICommand AddNewTestScoreCommand => new RelayCommand(AddTestScoreProc);
	    public Test SelectedTest { get; set; }
	    public ObservableCollection<string> SuggestedStudentsInString { get; set; }

	    public void AddTestScoreProc()
	    {

		    TestScore.StudentId = TestScore.Student.Id;
		    TestScore.TestId = SelectedTest.Id;
		    
		    CRUDTestScoreService.AddTestScore(TestScore);
			//App.Locator.TestDetailsViewModel.LoadTestScores();
			NavigationService.GoBack();
	    }

	    public void ConvertStringStudentsToActualStudents()
	    {

			var allStudents = CRUDStudentService.GetStudentsEnrolledInClass(CRUDClassService.GetSubjectFromId(SelectedTest.SubjectId));
			var suggestedStudents = new List<Student>();
		    foreach (var v in SuggestedStudentsInString)
		    {
			    suggestedStudents.Add(allStudents.FirstOrDefault(c => c.FullName == v));
		    }
			SuggestedStudents = new ObservableCollection<Student>(suggestedStudents);
	    }
    }

}
