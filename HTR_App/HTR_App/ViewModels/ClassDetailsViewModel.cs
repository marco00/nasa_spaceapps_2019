﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class ClassDetailsViewModel : HTRAppViewModelBase
    {
	    public ClassDetailsViewModel(INavigationService navigationService, ICRUDTestService crudTestService, ICRUDClassService crudClassService)
	    {
		    if (navigationService == null) throw new ArgumentNullException("navigationService");
		    NavigationService = (NavigationService)navigationService;
		    CRUDTestService = crudTestService;
		    CRUDClassService = crudClassService;
	    }

	    private ICRUDTestService CRUDTestService;

	    private ICRUDClassService CRUDClassService;

		public Subject SelectedSubject { get; set; } 
	    public ObservableCollection<Test> Tests { get; set; } = new ObservableCollection<Test>();
		public Test SelectedTest { get; set; }




	    public void LoadTests()
	    {
		    var templist = CRUDTestService.GetTests(SelectedSubject);
		    Tests.Clear();
		    foreach (var v in templist)
		    {
			    Tests.Add(v);
		    }
	    }
	    public RelayCommand GotoUpdateSubjectCommand => new RelayCommand(GotoUpdateSubject);

	    private void GotoUpdateSubject()
	    {
		    var parameters = new object[] {User, SelectedSubject};
		    NavigationService.NavigateTo(ViewModelLocator.UpdateSubjectPage, parameters, false);

	    }

	    public RelayCommand DeleteClassCommand => new RelayCommand(DeleteClass);

	    public void DeleteClass()
	    {
		    
		    CRUDClassService.DeleteSubject(SelectedSubject);
			NavigationService.GoBack();
		    
	    }

		public RelayCommand GotoTestDetailsPageCommand => new RelayCommand(GotoTestDetailsPageProc);

	    public void GotoTestDetailsPageProc()
	    {
		    var parameters = new object[] {User, SelectedTest};
		    NavigationService.NavigateTo(ViewModelLocator.TestDetailsPage, parameters, false);
	    }
	    

	    public RelayCommand GotoStudentsEnrolledViewCommand => new RelayCommand(GotoStudentsEnrolledViewProc);

	    private void GotoStudentsEnrolledViewProc()
	    {
		    var parameters = new object[] { User, SelectedSubject };
			NavigationService.NavigateTo(ViewModelLocator.StudentsEnrolledView, parameters, false);
	    }

		public RelayCommand GotoAddNewTestCommand => new RelayCommand(GotoAddNewTestProc);

	    private void GotoAddNewTestProc()
	    {
		    var parameters = new object[] { User, SelectedSubject };
			NavigationService.NavigateTo(ViewModelLocator.AddNewTestPage, parameters, false);
		}

		

		////////////////////////////////////
		/// 
		/// 


	}
}
