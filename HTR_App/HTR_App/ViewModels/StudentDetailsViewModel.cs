﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class StudentDetailsViewModel : HTRAppViewModelBase
    {
	    public StudentDetailsViewModel(INavigationService navigationService, ICRUDStudentService crudStudentService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDStudentService = crudStudentService;
	    }

	    private ICRUDStudentService CRUDStudentService;
	    private Student _selectedStudent;

	    public Student SelectedStudent
	    {
		    get { return _selectedStudent; }
		    set
		    {
			    _selectedStudent = value;
				RaisePropertyChanged(nameof(SelectedStudent));
		    }
	    }

	    public RelayCommand GotoUpdateStudentCommand => new RelayCommand(GotoUpdateStudent);

	    private void GotoUpdateStudent()
	    {
		    var parameters = new object[] {User, SelectedStudent};
		    NavigationService.NavigateTo(ViewModelLocator.UpdateStudentPage, parameters, false);

	    }


	    public RelayCommand DeleteStudentCommand => new RelayCommand(DeleteStudent);

	    public void DeleteStudent()
	    { 

		    CRUDStudentService.DeleteStudent(SelectedStudent); 
			NavigationService.GoBack();
		    
	    }
	}
}
