﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
	
	public class HomePageViewModel : HTRAppViewModelBase
	{
		public HomePageViewModel(INavigationService NavigationService, ICRUDClassService ICRUDClassService)
		{
			if (NavigationService == null) throw new ArgumentNullException("NavigationService");
			this.NavigationService = (NavigationService)NavigationService;
			this.ICRUDClassService = ICRUDClassService;
			
		}

		private ICRUDClassService ICRUDClassService;
		public ObservableCollection<Subject> Classes { get; set; } = new ObservableCollection<Subject>();
		//public ObservableCollection<Video> Videos { get; set; } = new ObservableCollection<Video>();
		public Subject SelectedSubject { get; set; }



		public void LoadClasses(User user)
		{
			var templist = ICRUDClassService.GetSubjects(user);
			Classes.Clear();
			foreach (var v in templist)
			{
				Classes.Add(v);
			}
		}

		public RelayCommand GoToAddNewSubjectCommand => new RelayCommand(GotoAddNewSubject);

		private void GotoAddNewSubject()
		{
			NavigationService.NavigateTo(ViewModelLocator.AddNewSubjectPage, User, false);
		}
		 
		public RelayCommand GotoClassDetailsPageCommand => new RelayCommand(GotoClassDetailsProc);

		public void GotoClassDetailsProc()
		{
			var parameters = new object[] {User, SelectedSubject};
			NavigationService.NavigateTo(ViewModelLocator.ClassDetailsPage, parameters, false);
		}

		
	}
}
