﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class StudentsListViewModel : HTRAppViewModelBase
    {

	    public StudentsListViewModel(INavigationService navigationService, ICRUDStudentService iCrudStudentService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDStudentService = iCrudStudentService;
	    }

	    private ICRUDStudentService CRUDStudentService;
	    public Student SelectedStudent { get; set; }
	    public Subject SelectedSubject { get; set; }
	    public ObservableCollection<Student> Students { get; set; } = new ObservableCollection<Student>();

	    public void LoadStudents()
	    {
		    Students.Clear();
		    var students = CRUDStudentService.GetStudentsEnrolledInClass(SelectedSubject);
		    foreach (var v in students)
		    {
			    Students.Add(v);
		    }
	    }

	    public RelayCommand GotoAddNewStudentCommand => new RelayCommand(GotoAddNewStudentProc);

	    private void GotoAddNewStudentProc()
	    {
		    var parameters = new object[] {User, SelectedSubject};
		    NavigationService.NavigateTo(ViewModelLocator.AddNewStudentPage, parameters, false);

	    }

	    public RelayCommand GotoStudentDetailsViewCommand => new RelayCommand(GotoStudentDetailsviewProc);

	    public void GotoStudentDetailsviewProc()
	    {
		    var parameters = new object[] {User, SelectedStudent};
		    NavigationService.NavigateTo(ViewModelLocator.StudentDetailsPage, parameters, false);
	    }
    }
}
