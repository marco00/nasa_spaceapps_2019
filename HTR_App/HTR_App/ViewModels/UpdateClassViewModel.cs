﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class UpdateClassViewModel : HTRAppViewModelBase
    {
	    public UpdateClassViewModel(INavigationService navigationService, ICRUDClassService classNavigationService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDClassService = classNavigationService;
			SubjectToReplaceOldSubject = new Subject();
	    }

	    private ICRUDClassService CRUDClassService;
	    public Subject SelectedSubject { get; set; }
	    public Subject SubjectToReplaceOldSubject { get; set; }
		public RelayCommand UpdateClassCommand => new RelayCommand(UpdateClass);

	    private void UpdateClass()
	    {
		    CRUDClassService.EditSubject(SelectedSubject, SubjectToReplaceOldSubject);
			App.Locator.ClassDetailsViewModel.SelectedSubject.Update(SubjectToReplaceOldSubject);
		    NavigationService.GoBack();
	    }
	}
}
