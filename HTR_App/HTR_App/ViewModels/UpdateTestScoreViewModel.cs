﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class UpdateTestScoreViewModel : HTRAppViewModelBase
    {
	    public UpdateTestScoreViewModel(INavigationService navigationService, ICRUDTestScoreService crudTestScoreService, ICRUDStudentService crudStudentService, ICRUDTestService crudTestService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDTestScoreService = crudTestScoreService;
		    CRUDStudentService = crudStudentService;
		    CRUDTestService = crudTestService;

	    }

	    public Test SelectedTest { get; set; }
	    public TestScore NewTestScore { get; set; }
	    public TestScore OldTestScore { get; set; }
	    public Student Student { get; set; }

		private ICRUDTestScoreService CRUDTestScoreService;
		private ICRUDStudentService CRUDStudentService;
		private ICRUDTestService CRUDTestService;

	    public void LoadTest()
	    {
		    SelectedTest = CRUDTestService.GetTestFromId(OldTestScore.TestId);
	    }
		public RelayCommand UpdateTestScoreCommand => new RelayCommand(UpdateTestScoreProc);

	    private void UpdateTestScoreProc()
	    {
		    CRUDTestScoreService.EditTestScore(OldTestScore, NewTestScore);
		    var scores = CRUDTestScoreService.GetTestScores(SelectedTest);
			NavigationService.GoBack();
	    }
		
	    public Student GetStudentFromId()
	    {
		    var studentId = OldTestScore.StudentId;
		    return CRUDStudentService.GetStudentFromId(studentId);
	    }

	    public void DeleteTestScore()
	    {
		    CRUDTestScoreService.DeleteTestScore(OldTestScore);
			NavigationService.GoBack();
		    
	    }
    }
}
