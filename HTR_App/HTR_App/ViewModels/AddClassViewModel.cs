﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class AddClassViewModel : HTRAppViewModelBase
    {
	    public AddClassViewModel(INavigationService navigationService, ICRUDClassService iCRUDClassService)
	    {
		    if (navigationService == null) throw new ArgumentNullException("navigationService");
			CRUDClassService = iCRUDClassService;
		    NavigationService = (NavigationService)navigationService;
			ClassToBeAdded = new Subject();
		    Mon = false;
		    Tue = false;
		    Wed = false;
		    Thu = false;
		    Fri = false;
		    Sat = false;
		    Sun = false;
		}

	    private ICRUDClassService CRUDClassService;
	    private Subject _classToBeAdded;
	    private TimeSpan _startTime;
	    private TimeSpan _endTime;
	    private bool _mon;
	    private bool _tue;
	    private bool _wed;
	    private bool _thu;
	    private bool _fri;
	    private bool _sat;
	    private bool _sun;

	    public Subject ClassToBeAdded
	    {
		    get { return _classToBeAdded; }
		    set
		    {
			    _classToBeAdded = value;
			    RaisePropertyChanged(nameof(ClassToBeAdded));
		    }
	    }

	    public TimeSpan StartTime
	    {
		    get { return _startTime; }
		    set
		    {
			    _startTime = value;
			    RaisePropertyChanged(nameof(StartTime));
		    }
	    }

	    public TimeSpan EndTime
	    {
		    get { return _endTime; }
		    set
		    {
			    _endTime = value;
			    RaisePropertyChanged(nameof(EndTime));
		    }
	    }

	    public bool Mon
	    {
		    get { return _mon; }
		    set
		    {
			    _mon = value;
			    RaisePropertyChanged(nameof(Mon));
		    }
	    }

	    public bool Tue
	    {
		    get { return _tue; }
		    set
		    {
			    _tue = value;
			    RaisePropertyChanged(nameof(Tue));
		    }
	    }

	    public bool Wed
	    {
		    get { return _wed; }
		    set
		    {
			    _wed = value;
			    RaisePropertyChanged(nameof(Wed));
		    }
	    }

	    public bool Thu
	    {
		    get { return _thu; }
		    set
		    {
			    _thu = value;
			    RaisePropertyChanged(nameof(Thu));
		    }
	    }

	    public bool Fri
	    {
		    get { return _fri; }
		    set
		    {
			    _fri = value;
			    RaisePropertyChanged(nameof(Fri));
		    }
	    }

	    public bool Sat
	    {
		    get { return _sat; }
		    set
		    {
			    _sat = value;
			    RaisePropertyChanged(nameof(Sat));
		    }
	    }

	    public bool Sun
	    {
		    get { return _sun; }
		    set
		    {
			    _sun = value;
			    RaisePropertyChanged(nameof(Sun));
		    }
	    }

	    public RelayCommand AddNewClassCommand => new RelayCommand(AddNewClassProc);

	    public void AddNewClassProc()
	    {

			ClassToBeAdded.TeacherId = User.Id;
		    string daysChosen = StringFromDayOfTheWeekBool();
		    ClassToBeAdded.Schedule = $"{StartTime} - {EndTime} ; {daysChosen}";
		    ClassToBeAdded.Code = ClassToBeAdded.Code.Trim();
		    ClassToBeAdded.Description = ClassToBeAdded.Description.Trim();
		    ClassToBeAdded.Name = ClassToBeAdded.Name.Trim();

		    if (String.IsNullOrEmpty(ClassToBeAdded.Code)) return;
		    if (String.IsNullOrEmpty(ClassToBeAdded.Description)) return;
		    if (String.IsNullOrEmpty(ClassToBeAdded.Name)) return;
		    if (Sun == false && Mon == false && Tue == false && Wed == false && Thu == false && Fri == false &&
		        Sat == false) return;

			CRUDClassService.AddClass(ClassToBeAdded);
			NavigationService.GoBack();
		}

	    private string StringFromDayOfTheWeekBool()
	    {
		    var output = "";
		    if (Mon == true) output += "M, ";
		    if (Tue == true) output += "T, ";
		    if (Wed == true) output += "W, ";
		    if (Thu == true) output += "Th, ";
		    if (Fri == true) output += "F, ";
		    if (Sat == true) output += "Sat, ";
		    if (Sun == true) output += "Sun, ";
		    output = output.Remove(output.Length - 2);
		    return output;
	    }

	    public void InitializeFields()
	    {
		    ClassToBeAdded = new Subject();
			StartTime = new TimeSpan();
			EndTime = new TimeSpan();
		    Mon = false;
		    Tue = false;
		    Wed = false;
		    Thu = false;
		    Fri = false;
		    Sat = false;
		    Sun = false;
		}
    }
}
