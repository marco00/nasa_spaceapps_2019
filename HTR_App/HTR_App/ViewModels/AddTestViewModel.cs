﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class AddTestViewModel : HTRAppViewModelBase
    {
	    public AddTestViewModel(INavigationService navigationService, ICRUDTestService iCRUDTestService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDTestService = iCRUDTestService;
		    
	    }

	    private ICRUDTestService CRUDTestService;
	    public Subject SelectedSubject { get; set; }
	    public Test TestToBeAdded { get; set; }

	    public RelayCommand AddNewTestCommand => new RelayCommand(AddNewTestProc);

	    public void AddNewTestProc()
	    {
		    if (String.IsNullOrEmpty(TestToBeAdded.Category)) return;
		    if (String.IsNullOrEmpty(TestToBeAdded.Name)) return;
			//TODO Add datetime selector in XAML and replace next line

		    TestToBeAdded.Category = TestToBeAdded.Category.Trim();
		    TestToBeAdded.Name = TestToBeAdded.Name.Trim();

			TestToBeAdded.DateTimeTaken = DateTime.Now;
		    TestToBeAdded.SubjectId = SelectedSubject.Id;
		    TestToBeAdded.TeacherId = User.Id;
		    TestToBeAdded.DateTimeTaken = DateTime.Now;
			CRUDTestService.AddTest(TestToBeAdded);
			NavigationService.GoBack();
			
		}
	}
}
