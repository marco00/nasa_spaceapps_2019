﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class UpdateStudentViewModel : HTRAppViewModelBase
    {
	    public UpdateStudentViewModel(INavigationService navigationService, ICRUDStudentService crudStudentService)
	    {
		    if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDStudentService = crudStudentService;
	    }

	    private ICRUDStudentService CRUDStudentService;
	    public Student SelectedStudent { get; set; }
	    public Student StudentToReplaceOldStudent { get; set; }

	    public RelayCommand UpdateStudentCommand => new RelayCommand(UpdateStudent);

	    private void UpdateStudent()
	    {
		    CRUDStudentService.EditStudent(SelectedStudent, StudentToReplaceOldStudent);
		    var vm = App.Locator.StudentDetailsViewModel;
		    vm.SelectedStudent = StudentToReplaceOldStudent.ShallowCopy();
			NavigationService.GoBack();
	    }
	}
}
