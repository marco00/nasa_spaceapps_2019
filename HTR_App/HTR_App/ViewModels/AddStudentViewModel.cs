﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;

namespace HTR_App.ViewModels
{
    public class AddStudentViewModel : HTRAppViewModelBase
    {
	    public AddStudentViewModel(INavigationService navigationService, ICRUDStudentService iCRUDStudentService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDStudentService = iCRUDStudentService;
	    }

	    private ICRUDStudentService CRUDStudentService;
	    public Subject SelectedSubject { get; set; }
		public RelayCommand AddNewStudentCommand => new RelayCommand(AddNewStudentProc);
	    public Student StudentToBeAdded { get; set; }
	    public string ContactNumber { get; set; }

	    public void AddNewStudentProc()
	    {
		    if (String.IsNullOrEmpty(StudentToBeAdded.Email)) return;
		    if (String.IsNullOrEmpty(StudentToBeAdded.FirstName)) return;
		    if (String.IsNullOrEmpty(StudentToBeAdded.LastName)) return;
		    if (String.IsNullOrEmpty(ContactNumber)) return;

		    StudentToBeAdded.Email = StudentToBeAdded.Email.Trim();
		    StudentToBeAdded.FirstName = StudentToBeAdded.FirstName.Trim();
		    StudentToBeAdded.LastName = StudentToBeAdded.LastName.Trim();

		    StudentToBeAdded.ContactNumber = long.Parse(ContactNumber);
			CRUDStudentService.AddStudent(StudentToBeAdded, SelectedSubject, User);
		    NavigationService.GoBack();
	    }


	}
}
