﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HTR_App.Helpers;
using HTR_App.Models;
using HTR_App.Services;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

namespace HTR_App.ViewModels
{
    public class TestDetailsViewModel : HTRAppViewModelBase
    {
	    public TestDetailsViewModel(INavigationService navigationService, ICRUDTestService crudTestService, ICRUDTestScoreService crudTestScoreService, ICRUDStudentService crudStudentService, ICRUDClassService crudClassService)
	    {
			if (navigationService == null) throw new ArgumentNullException("NavigationService");
		    this.NavigationService = (NavigationService)navigationService;
		    CRUDTestService = crudTestService;
		    CRUDTestScoreService = crudTestScoreService;
		    CRUDStudentService = crudStudentService;
		    CRUDClassService = crudClassService;
			HTRService = new HTRService();
		    TestImageSource = "/storage/emulated/0/Android/data/com.companyname.HTR_App/files/Pictures/Sample/test.jpg";
		    IsBusy = false;
	    }

	    private ICRUDTestService CRUDTestService;
	    private ICRUDTestScoreService CRUDTestScoreService;
	    private ICRUDStudentService CRUDStudentService;
	    private ICRUDClassService CRUDClassService;
	    private HTRService HTRService;
	    private string _testImageSource;
	    private bool _isBusy;
	    public ObservableCollection<TestScore> TestScores { get; set; } = new ObservableCollection<TestScore>();
	    public Test SelectedTest { get; set; }
	    public TestScore SelectedTestScore { get; set; }

	    public string TestImageSource
	    {
		    get { return _testImageSource; }
		    set
		    {
			    _testImageSource = value;
			    RaisePropertyChanged(nameof(TestImageSource));
		    }
	    }

	    public bool IsBusy
	    {
		    get { return _isBusy; }
		    set
		    {
			    _isBusy = value;
			    RaisePropertyChanged(nameof(IsBusy));
		    }
	    }

	    public void LoadTestScores()
	    {
		    var testScores = CRUDTestScoreService.GetTestScores(SelectedTest);
			TestScores.Clear();
		    foreach (var v in testScores)
		    {
			    var student = CRUDStudentService.GetStudentFromId(v.StudentId);
			    v.Student = student;
				TestScores.Add(v);
		    }
	    }
		public RelayCommand TakePictureCommand => new RelayCommand(TakePictureProc);

	    private void TakePictureProc()
	    {
			//TODO HTR 
		    var subjectId = SelectedTest.SubjectId;
		    var subject = CRUDClassService.GetSubjectFromId(subjectId);
		    var studentsEnrolled = CRUDStudentService.GetStudentsEnrolledInClass(subject);

		    foreach (var v in studentsEnrolled)
		    {
				var newTestScore1 = new TestScore()
				{
					NumberOfItems = 10,
					Score = 10,
					Student = v,
					StudentId = v.Id,
					TestId = SelectedTest.Id
				};
				CRUDTestScoreService.AddTestScore(newTestScore1);
				TestScores.Add(newTestScore1);
			}
			
		}
		

	    public RelayCommand DeleteTestCommand => new RelayCommand(DeleteTest);

	    public void DeleteTest()
	    {
		    CRUDTestService.DeleteTest(SelectedTest);
			NavigationService.GoBack();
	    }

	    public void GotoUpdateTestScore()
	    {
			NavigationService.NavigateTo(ViewModelLocator.UpdateTestScorePage, new object[] { User, SelectedTestScore }, false);
		}

	    public ObservableCollection<string> SuggestedNames { get; set; } = new ObservableCollection<string>();
	    public int ScoreNumerator { get; set; }
	    public int ScoreDenominator { get; set; } 
	    public void ScanImage(TextOperationResult result)
	    {
			var correctiveAlgorithms = new CorrectiveAlgorithms();
		    
		    var lines = result.RecognitionResult.Lines;
		    foreach (var v in lines)
		    {
			    var nameOrScore = correctiveAlgorithms.IsNameOrScore(v.Text);
			    if (nameOrScore == 1)
			    {
					//score
				    var probability = correctiveAlgorithms.HowLikelyToBeAValidScore(v.Text);
				    if (probability == 1)
				    {
					    ScoreNumerator = int.Parse(v.Text.Split('/')[0]);
					    ScoreDenominator = int.Parse(v.Text.Split('/')[1]);
					}
			    }
				else if (nameOrScore == 0)
			    {
					//name
				    var listOfEnrolledStudents =
					    CRUDStudentService.GetStudentsEnrolledInClass(CRUDClassService.GetSubjectFromId(SelectedTest.SubjectId));
;				    var listOfEnrolledStudentsInString = new List<string>();
				    foreach (var a in listOfEnrolledStudents)
				    {
					    listOfEnrolledStudentsInString.Add(a.FullName);
				    }
				    bool clearSuggestedNames = false;
				    var suggestions = correctiveAlgorithms.AutoComplete(v.Text, listOfEnrolledStudentsInString, out clearSuggestedNames);
				    if (clearSuggestedNames)
				    {
					    SuggestedNames.Clear();
					    SuggestedNames = new ObservableCollection<string>(suggestions);
					}
					
			    }
			    if (nameOrScore == 2)
			    {
				    if (ScoreNumerator == 0 && ScoreDenominator == 0)
				    {
					    ScoreNumerator = int.Parse(v.Text);
					    return;
				    }
				    if (ScoreDenominator == 0)
				    {
					    ScoreDenominator = int.Parse(v.Text);
					    return;
				    }
			    }

		    }

		    var parameters = new object[] {SelectedTest, User, SuggestedNames, ScoreNumerator, ScoreDenominator};
		    IsBusy = false;
		    
		    NavigationService.NavigateTo(ViewModelLocator.AddNewTestScorePage, parameters, false);
	    }
    }
}
