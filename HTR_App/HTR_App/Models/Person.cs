﻿using System;
using SQLite;

namespace HTR_App.Models
{
	public class Person
	{
		
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string FullName => $"{FirstName} {LastName}";
		public int Age { get; set; }
		public DateTime Birthdate { get; set; }
		public string Address { get; set; }
		public long ContactNumber { get; set; }

		public override string ToString()
		{
			return $"{FirstName} {LastName}";
		}
	}



}