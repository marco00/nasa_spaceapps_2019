﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HTR_App.Models.NasaApiModels
{
    public class EarthObject
    {
        public decimal cloud_score { get; set; }
        public string date { get; set; }
        public string id { get; set; }
        public string url { get; set; }

    }
}
