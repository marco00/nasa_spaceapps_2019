﻿using System.Collections.Generic;

namespace HTR_App.Models.NasaApiModels
{
    public class RootObject
    {
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public List<event_> events { get; set; }


    }
}