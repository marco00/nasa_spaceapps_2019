﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HTR_App.Models.NasaApiModels
{
    public class Update
    {
        public Image Image { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int NumberOfLikes { get; set; }

    }
}
