﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HTR_App.Models.NasaApiModels
{
    public class geometry
    {
        public string date { get; set; }
        public string type { get; set; }
        public decimal[] coordinates { get; set; }
    }
}
