﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using NUnit.Framework.Internal.Execution;

namespace HTR_App.Models.NasaApiModels
{
    public class event_
    {
        public int Id { get; set; }
        public string id { get; set; }
        public string title { get; set;}
        public string description { get; set; }
        public string link { get; set; }
        public List<category> categories { get; set; }
        public List<source> sources { get; set; }
        public List<geometry> geometries { get; set; }
    }
}
