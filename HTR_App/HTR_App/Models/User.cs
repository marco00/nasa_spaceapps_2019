﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using HTR_App.Helpers;
using SQLite;

namespace HTR_App.Models
{
	[Table("User")]
	public class User :Person
	{

		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }

		//public string FirstName { get; set; }
		//public string LastName { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string ReTypePassword { get; set; }

	}
}
