﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace HTR_App.Models
{
	[Table("TestScore")]
	public class TestScore
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int TestScoreId { get; set; }
		public int TestId { get; set; }
		public int StudentId { get; set; }
		public int Score { get; set; }
		public int NumberOfItems { get; set; }
		[Ignore]
		public Student Student { get; set; }
		[Ignore]
		public string TestScoreInString => $"{Score}/{NumberOfItems}";

		public TestScore ShallowCopy()
		{
			return new TestScore()
			{
				NumberOfItems = NumberOfItems,
				Score = Score,
				StudentId = StudentId,
				TestId = TestId,
				TestScoreId = TestScoreId
			};
		}
	}
}
