﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace HTR_App.Models
{
	[Table("Subject")]
	public class Subject
    {
	    [PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }
		public int TeacherId { get; set; }
		public string Name { get; set; }
	    public string Code { get; set; }
	    public string Description { get; set; }
	    public string Schedule { get; set; }

	    public Subject ShallowCopy()
	    {
		    return new Subject()
		    {
			    Id = Id,
				TeacherId = TeacherId,
				Name = Name,
				Code = Code,
				Description = Description,
				Schedule = Schedule
		    };
	    }

	    public void Update(Subject subject)
	    {
		    Name = subject.Name;
		    Code = subject.Code;
		    Description = subject.Description;
		    Schedule = subject.Schedule;
	    }
	    public override string ToString()
	    {
		    return Name;
	    }
    }
}
