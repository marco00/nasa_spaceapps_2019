﻿using System;
using SQLite;

namespace HTR_App.Models
{
	[Table("Test")]
	public class Test
	{
		public string Name { get; set; }
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }
		public DateTime DateTimeTaken { get; set; }
		public int SubjectId { get; set; }
		public int TeacherId { get; set; }
		public string Category { get; set; }

		public Test ShallowCopy()
		{
			return new Test
			{
				Name = Name,
				Id = Id,
				DateTimeTaken = DateTimeTaken,
				SubjectId = SubjectId,
				TeacherId = TeacherId,
				Category = Category
			};
		}

		public void Update(Test test)
		{
			Name = test.Name;
		}
	}
}