﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HTR_App.Models
{
    
	public class Word
	{
		public List<int> boundingBox { get; set; }
		public string text { get; set; }
	}

	public class Line
	{
		public List<int> boundingBox { get; set; }
		public string text { get; set; }
		public List<Word> words { get; set; }
	}

	public class RecognitionResult
	{
		public List<Line> lines { get; set; }
	}

	public class RootObject
	{
		public string status { get; set; }
		public bool succeeded { get; set; }
		public bool failed { get; set; }
		public bool finished { get; set; }
		public RecognitionResult recognitionResult { get; set; }
	}
}
