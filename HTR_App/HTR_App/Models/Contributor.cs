﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTR_App.Models
{
    public class Contributor : Person, IComparable
    {
        public float FundsDonated { get; set; }
        public int CompareTo(object obj)
        {
            var contributor = obj as Contributor;
            if(contributor.FundsDonated > FundsDonated)
            {
                return 1;
            }
            if(contributor.FundsDonated < FundsDonated)
            {
                return -1;
            }

            return 0;
        }
    }
}
