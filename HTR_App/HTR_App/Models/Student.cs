﻿using System;
using SQLite;

namespace HTR_App.Models
{
	public class Student : Person
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }

		public Student ShallowCopy()
		{
			return new Student()
			{
				Email = Email,
				FirstName = FirstName,
				LastName = LastName,
				Age = Age,
				Birthdate = Birthdate,
				Address = Address,
				ContactNumber = ContactNumber,
				Id = Id
			};
		}

		public override string ToString()
		{
			return FullName;
		}

		public void Update(Student student)
		{
			//Email = student.Email;
			//FirstName = student.FirstName;
			//LastName = student.LastName;
			//Age = student.Age;
			//Birthdate = student.Birthdate;
			//ContactNumber = student.ContactNumber;
		}
	}
}