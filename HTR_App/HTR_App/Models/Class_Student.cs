﻿using SQLite;
using Xamarin.Forms;

namespace HTR_App.Models
{
	//[Table("Class_Student")]
	//public class Video
	//{
	//	public int id { get; set; }
	//	public string Name { get; set; }
	//	public string URL { get; set; }
	//	public Image Thumbnail { get; set; }
	//}
	[Table("Class_Student")]
	public class Class_Student
	{

		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }
		public int ClassId { get; set; }
		public int StudentId { get; set; }
	}
}