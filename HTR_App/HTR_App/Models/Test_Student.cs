﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace HTR_App.Models
{
	[Table("Test_Student")]
    public class Test_Student
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }
		public int TestId { get; set; }
	    public int StudentId { get; set; }
	}
}
