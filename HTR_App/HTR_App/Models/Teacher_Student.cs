﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace HTR_App.Models
{
	[Table("Teacher_Student")]
    public class Teacher_Student
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }
		public int TeacherId { get; set; }
	    public int StudentId { get; set; }
	}
}
